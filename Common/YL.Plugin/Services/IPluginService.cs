﻿using System.IO;
using System.Threading.Tasks;

namespace YL.Plugin.Services
{
    public interface IPluginService
    {
        Task Add(string name,Stream stream);
        Task Remove(string name);
        Task Update(string name,Stream stream);
        Task<dynamic> Enables();
    }
}