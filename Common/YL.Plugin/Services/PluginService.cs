﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using YL.Plugin.Data;
using YL.Plugin.Database.Model;

namespace YL.Plugin.Services
{
    public class PluginService: IPluginService
    {
        private readonly IPluginPackageProvider _pluginPackageProvider;
        private readonly IPluginsLoadManager _pluginsLoadManager;
        private readonly PluginRepository _pluginRepository;
        public PluginService(IPluginPackageProvider pluginPackageProvider, IPluginsLoadManager pluginsLoadManager, PluginRepository pluginRepository)
        {
            _pluginPackageProvider = pluginPackageProvider;
            _pluginsLoadManager = pluginsLoadManager;
            _pluginRepository = pluginRepository;
        }
        public Task Add(string name,Stream stream)
        {
            try
            {
                _pluginPackageProvider.Load(name, stream);
                var config = _pluginPackageProvider.GetConfiguration();
                _pluginsLoadManager.Enable(name, config.TempPath);
                _pluginPackageProvider.SetupFolder();
                return _pluginRepository.Add(new Plugins
                {
                    Enable = true,
                    Path = config.Path,
                    Name = config.Name,
                    UniqueKey = config.UniqueKey,
                    Version = config.Version.VersionNumber,
                    
                });
            }
            catch (Exception e)
            {
                _pluginPackageProvider.RemoveFolder();
                throw;
            }
        }

        public Task Remove(string name)
        {
            _pluginsLoadManager.Disable(name);
            return _pluginRepository.Disable(name);
        }

        public Task Update(string name,Stream stream)
        {
            try
            {
                _pluginsLoadManager.Disable(name);
                _pluginPackageProvider.Load(name, stream);
                var config = _pluginPackageProvider.GetConfiguration();
                _pluginsLoadManager.Enable(name, config.TempPath);
                _pluginPackageProvider.SetupFolder();
                return _pluginRepository.Update(name, config.Version.VersionNumber);
            }
            catch (Exception e)
            {
                _pluginPackageProvider.RemoveFolder();
                throw;
            }
        }

        public async Task<dynamic> Enables()
        {
            var enables = await _pluginRepository.GetEnables();
            var list = new List<dynamic>();
            enables.ForEach(p =>
            {
                list.Add(new
                {
                    Name = p.Name,
                    Enable = p.Enable,
                    CreateTime = p.CreateTime,
                    UpdateTime = p.UpdateTime,
                    Path = p.Path,
                    Version = p.Version
                });
            });
            return Task.FromResult(list);
        }
    }
}