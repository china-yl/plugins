﻿using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using YL.Plugin.Core;
using YL.Plugin.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using YL.Plugin.Providers;
using Microsoft.AspNetCore.Mvc.Controllers;
using YL.Plugin.Services;

namespace YL.Plugin.Extensions
{
    public static class StartupExtension
    {
        public static IServiceCollection AddPlugin(this IServiceCollection services,string connectString)
        {
            services.AddOptions();
            services.AddEntityFrameworkMySql()
                .AddDbContext<PluginDbContext>(options => options.UseMySql(connectString));
            services.AddTransient<PluginDbContext>();
            services.AddTransient<PluginRepository>();
            services.AddTransient<IPluginsLoadManager, PluginsLoadManager>();
            services.AddTransient<IPluginService, PluginService>();
            services.AddTransient<IPluginPackageProvider, PluginPackageProvider>();
            services.AddSingleton<IActionDescriptorChangeProvider>(PluginActionDescriptorChangeProvider.Instance);
            services.AddSingleton(PluginActionDescriptorChangeProvider.Instance);
            var provider = services.BuildServiceProvider();
            using (var scope = provider.CreateScope())
            {
                var context = new PluginAssemblyLoadContext();
                var pluginRepository = scope.ServiceProvider.GetService<PluginRepository>();
                var mvcBuilder = scope.ServiceProvider.GetService<IMvcBuilder>();
                var enables = pluginRepository.GetEnables().Result;
                if (enables == null) return services;
                foreach (var enable in enables)
                {
                    var name = enable.Name;
                    if (!File.Exists(enable.Path)) continue;
                    using var fs = new FileStream(enable.Path, FileMode.Open);
                    var assembly = context.LoadFromStream(fs);

                    var controllerAssemblyPart = new AssemblyPart(assembly);
                    mvcBuilder.PartManager.ApplicationParts.Add(controllerAssemblyPart);
                    PluginsLoadContext.Add(name, context);
                }
            }
            return services;
        }
    }
}
