﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace YL.Plugin.Extensions
{
    internal static class PluginExtension
    {
        public static string CurrentDirectoryPath = AppDomain.CurrentDomain.BaseDirectory; 
        public static string DllPath(this string moduleName,string version)
        {
            return $"{CurrentDirectoryPath}{BaseConfig.Modules}\\{moduleName}\\{version}\\{moduleName}.dll";
        }
        public static string Path(this string moduleName)
        {
            return $"{CurrentDirectoryPath}{BaseConfig.Modules}\\{moduleName}";
        }
        public static string Path(this string moduleName,string version)
        {
            return $"{CurrentDirectoryPath}{BaseConfig.Modules}\\{moduleName}\\{version}";
        }

        public static string TempPath(string name)
        {
            return $"{CurrentDirectoryPath}{BaseConfig.Temp}\\{name}";
        }
    }
}
