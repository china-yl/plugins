﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using YL.Plugin;
using YL.Plugin.Core;
using YL.Plugin.Providers;
using YL.Plugin.Services;

namespace YL.Plugin
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PluginController : ControllerBase
    {
        private readonly IPluginService _pluginService;
        public PluginController(IPluginService pluginService)
        {
            _pluginService = pluginService;
        }

        [HttpPost]
        public async Task<IActionResult> Add(IFormFile file)
        {
            var stream = file.OpenReadStream();
            await _pluginService.Add(Path.GetFileNameWithoutExtension(file.FileName),stream);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Remove(string name)
        {
            await _pluginService.Remove(name);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Update(IFormFile file)
        {
            var stream = file.OpenReadStream();
            await _pluginService.Update(Path.GetFileNameWithoutExtension(file.FileName), stream);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Enables()
        {
            var result = await _pluginService.Enables();
            return Ok(result);
        }
    }
}
