﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Plugin.Model
{
    public class PluginConfiguration
    {
        public string Name { get; set; }

        public string UniqueKey { get; set; }

        public string Path { get; set; }
        public string TempPath { get; set; }

        public Version Version { get; set; }
    }
}
