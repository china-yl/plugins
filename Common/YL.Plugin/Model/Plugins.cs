﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Plugin.Database.Model
{
    public class Plugins
    {
        public int Id { get; set; }
        public string UniqueKey { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Version { get; set; }
        public bool Enable { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
