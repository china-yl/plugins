﻿using System.IO;
using YL.Plugin.Model;

namespace YL.Plugin
{
    public interface IPluginPackageProvider
    {
        void Load(string name,Stream stream);
        PluginConfiguration GetConfiguration();
        void SetupFolder();
        void RemoveFolder();
    }
}