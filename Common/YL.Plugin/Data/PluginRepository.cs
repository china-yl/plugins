﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YL.Plugin.Database.Model;

namespace YL.Plugin.Data
{
    public class PluginRepository
    {
        private readonly DbSet<Plugins> _db;
        private readonly PluginDbContext _pluginDbContext;
        public PluginRepository(PluginDbContext pluginDbContext)
        {
            _db = pluginDbContext.Plugins;
            _pluginDbContext = pluginDbContext;
        }

        public Task<List<Plugins>> GetEnables()
        {
            var list = _db.Where(p => p.Enable);
            return Task.FromResult(list.ToList());
        }

        public Task Update(string name,string version)
        {
            var plugin = _db.FirstOrDefault(p => p.Name == name);
            if (plugin == null) return null;
            plugin.Version = version;
            plugin.UpdateTime = DateTime.Now;
            _db.Update(plugin);
            return _pluginDbContext.SaveChangesAsync();
        }

        public Task Add(Plugins plugin)
        {
            plugin.CreateTime = DateTime.Now;
            plugin.UpdateTime = DateTime.Now;
            _db.AddAsync(plugin);
            return _pluginDbContext.SaveChangesAsync();
        }

        public Task Disable(string name)
        {
            var plugin = _db.FirstOrDefault(p => p.Name == name);
            if (plugin == null) return null;
            plugin.Enable = false;
            plugin.UpdateTime = DateTime.Now;
            _db.Update(plugin);
            return _pluginDbContext.SaveChangesAsync();
        }
    }
}
