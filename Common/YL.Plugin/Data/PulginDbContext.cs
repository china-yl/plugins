﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using YL.Plugin.Database.Model;

namespace YL.Plugin.Data
{
    public class PluginDbContext: DbContext
    {
        public DbSet<Plugins> Plugins { get; set; }

        public PluginDbContext(DbContextOptions<PluginDbContext> options) : base(options)
        {

        }
    }
}
