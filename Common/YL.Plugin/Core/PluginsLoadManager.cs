﻿using Microsoft.AspNetCore.Mvc.ApplicationParts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YL.Plugin.Extensions;
using YL.Plugin.Providers;

namespace YL.Plugin.Core
{
    public class PluginsLoadManager : IPluginsLoadManager
    {
        private readonly ApplicationPartManager _partManager;
        public PluginsLoadManager(ApplicationPartManager partManager)
        {
            _partManager = partManager;
        }

        public void Disable(string name)
        {
            var last = _partManager.ApplicationParts.First(p => p.Name == name);
            _partManager.ApplicationParts.Remove(last);

            ResetControllActions();

            PluginsLoadContext.Remove(name);
        }

        public void Enable(string name,string path)
        {
            if (!PluginsLoadContext.Contains(name))
            {
                var context = new PluginAssemblyLoadContext();
                var file = Path.Combine(path, name) + ".dll";
                using var fs = new FileStream(file, FileMode.Open);
                var assembly = context.LoadFromStream(fs);

                var controllerAssemblyPart = new AssemblyPart(assembly);

                _partManager.ApplicationParts.Add(controllerAssemblyPart);
                PluginsLoadContext.Add(name, context);
            }
            else
            {
                var context = PluginsLoadContext.Get(name);
                var controllerAssemblyPart = new AssemblyPart(context.Assemblies.First());
                _partManager.ApplicationParts.Add(controllerAssemblyPart);
            }

            ResetControllActions();
        }

        private void ResetControllActions()
        {
            PluginActionDescriptorChangeProvider.Instance.HasChanged = true;
            PluginActionDescriptorChangeProvider.Instance.TokenSource.Cancel();
        }
    }
}
