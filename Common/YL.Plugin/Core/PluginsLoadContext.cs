﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Plugin.Core
{
    internal class PluginsLoadContext
    {
        private static Dictionary<string, PluginAssemblyLoadContext> _pluginContexts = null;
        static PluginsLoadContext()
        {
            _pluginContexts = new Dictionary<string, PluginAssemblyLoadContext>();
        }

        public static bool Contains(string pluginName)
        {
            return _pluginContexts.ContainsKey(pluginName);
        }

        public static void Remove(string pluginName)
        {
            if (_pluginContexts.ContainsKey(pluginName))
            {
                _pluginContexts[pluginName].Unload();
                _pluginContexts.Remove(pluginName);
            }
        }
        public static PluginAssemblyLoadContext Get(string pluginName)
        {
            return _pluginContexts[pluginName];
        }

        public static void Add(string pluginName, PluginAssemblyLoadContext context)
        {
            _pluginContexts.Add(pluginName, context);
        }
    }
}
