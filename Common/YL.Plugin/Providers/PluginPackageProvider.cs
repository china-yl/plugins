﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using ZipTool = System.IO.Compression.ZipArchive;
using System.Linq;
using System.Text;
using YL.Plugin.Model;
using YL.Plugin.Extensions;

namespace YL.Plugin.Providers
{
    public class PluginPackageProvider: IPluginPackageProvider
    {
        private PluginConfiguration _pluginConfiguration = null;
        private Stream _zipStream = null;
        private string _folderName = string.Empty;
        private string _name = string.Empty;

        public PluginPackageProvider()
        {
        }

        public void Load(string name,Stream stream)
        {
            _zipStream = stream;
            _name = name;
            var unionKey = Guid.NewGuid().ToString();
            var tempFolder = PluginExtension.TempPath(_name);
            var archive = new ZipTool(_zipStream, ZipArchiveMode.Read);

            archive.ExtractToDirectory(tempFolder);
            var fileName = Path.Combine(tempFolder, _name) + ".dll";

            if (string.IsNullOrEmpty(fileName))
            {
                throw new Exception("The plugin is missing the configuration file.");
            }
            else
            {
                var info = System.Diagnostics.FileVersionInfo.GetVersionInfo(fileName);
                _pluginConfiguration = new PluginConfiguration {
                    Path = _name.DllPath(info.FileVersion),
                    Name = _name,
                    UniqueKey = unionKey,
                    Version = info.FileVersion,
                    TempPath = tempFolder
                };
            }
        }

        public PluginConfiguration GetConfiguration()
        {
            return _pluginConfiguration;
        }

        public void SetupFolder()
        {
            ZipTool archive = new ZipTool(_zipStream, ZipArchiveMode.Read);
            _zipStream.Position = 0;
            Clear(_pluginConfiguration.Name);
            archive.ExtractToDirectory(_pluginConfiguration.Path, true);

            var folder = new DirectoryInfo(_pluginConfiguration.TempPath);
            folder.Delete(true);
        }

        public void RemoveFolder()
        {
            var folder = new DirectoryInfo(_pluginConfiguration.TempPath);
            folder.Delete(true);
        }

        private void Clear(string name)
        {
            var path = name.Path();

            DirectoryInfo root = new DirectoryInfo(path);
            var dirs = root.GetDirectories();
            var list = dirs.Select(p => p.Name).OrderByDescending(p => p).ToList();
            list = list.Skip(5).ToList();
            if (list.Any())
            {
                list.ForEach(p =>
                {
                    var folder = new DirectoryInfo(name.Path(p));
                    folder.Delete(true);
                });
            }
        }
    }
}
