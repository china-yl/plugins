﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Plugin
{
    public interface IPluginsLoadManager
    {
        void Disable(string name);
        void Enable(string name,string path);
    }
}
