﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Text;

namespace YL.PluginUI
{
    public static class PluginUIBuilderExtensions
    {
        public static IApplicationBuilder UsePluginUI(
            this IApplicationBuilder app,
            Action<PluginUIOptions> setupAction = null)
        {
            if (setupAction == null)
            {
                // Don't pass options so it can be configured/injected via DI container instead
                app.UseMiddleware<PluginUIMiddleware>();
            }
            else
            {
                // Configure an options instance here and pass directly to the middleware
                var options = new PluginUIOptions();
                setupAction.Invoke(options);

                app.UseMiddleware<PluginUIOptions>(options);
            }

            return app;
        }
    }
}
