﻿using System;
using System.IO;
using System.Reflection;

namespace YL.PluginUI
{
    public class PluginUIOptions
    {
        /// <summary>
        /// Gets or sets a route prefix for accessing the plugin-ui
        /// </summary>
        public string RoutePrefix { get; set; } = "plugin";

        /// <summary>
        /// Gets or sets a Stream function for retrieving the plugin-ui page
        /// </summary>
        public Func<Stream> IndexStream { get; set; } = () => typeof(PluginUIOptions).GetTypeInfo().Assembly
            .GetManifestResourceStream("YL.PluginUI.index.html");

        /// <summary>
        /// Gets or sets a title for the plugin-ui page
        /// </summary>
        public string DocumentTitle { get; set; } = "Plugin UI";

        public string HeadContent { get; set; }
    }
}
