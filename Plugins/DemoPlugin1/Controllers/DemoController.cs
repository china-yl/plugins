﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoPlugin1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DemoController:ControllerBase
    {
        [HttpGet]
        public string Ping(string name)
        {
            return $"{name} hell888!";
        }
    }
}
