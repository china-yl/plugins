﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using PluginLoad.Filters;
using Swashbuckle.AspNetCore.Swagger;
using YL.Plugin.Extensions;
using YL.Plugin.Providers;
using YL.PluginUI;

namespace PluginLoad
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
                options.AddPolicy("plugin", p => p.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                )
            );
            services.AddWebapiSwaggerGen();
            services.AddControllers()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddPlugin(Configuration.GetConnectionString("PulginDbContext"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("plugin");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UsePluginUI();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Plugin Web API");
                c.ShowExtensions();
            });
            app.UseRouting().UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());
        }
    }


    public static class StartupExtension
    {
        public static IServiceCollection AddWebapiSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Plugin接口文档",
                    Description = "Plugin接口文档",
                    Contact = new OpenApiContact { Name = "Plugin", Email = "906469935@qq.com"}
                });

                //Set the comments path for the swagger json and ui.
                var documentFiles = GetDocumentList();
                if (documentFiles != null)
                {
                    foreach (var item in documentFiles)
                    {
                        c.IncludeXmlComments(item);
                    }
                }
            });
            services.ConfigureSwaggerGen(option =>
            {
                option.OperationFilter<SwaggerFileUploadFilter>();
            });
            return services;
        }
        /// <summary>
        /// 获取Document文档注释
        /// </summary>
        /// <returns></returns>
        private static string[] GetDocumentList()
        {
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            return Directory.GetFiles(basePath, "*.xml");
        }
    }
}
